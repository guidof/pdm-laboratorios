package com.example.guido.formularios_eventos;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
    private Button botonVolver;
    private Button botonSalvar;
    private ImageButton imagebutton;
    private TextView titulo;  //texto: formulario
    private TextView texto1;
    private TextView texto2;
    private TextView texto3;
    private TextView texto4;
    private EditText textoEditable;
    private CheckBox checkBox;
    private RadioButton radio1;
    private RadioButton radio2;
    private ToggleButton toggleButton;
    private RatingBar ratingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        setUpViews(); //metodo que configura las vistas

    }

    public void checkBoxClick(View w){
        String text = "";
        if (checkBox.isChecked()) {
            text = "Selected";
            botonSalvar.setEnabled(true);
            Toast.makeText(this,"Ya puedes Salvar", Toast.LENGTH_LONG).show();
        } else {
            botonSalvar.setEnabled(false);
            Toast.makeText(this, "Hasta que no marques la casilla no podrás salvar",Toast.LENGTH_LONG).show();
            text = "Not selected";
        }
        Toast.makeText(this,text, Toast.LENGTH_SHORT).show();
    }

    public void setUpViews(){
        botonVolver = (Button) findViewById(R.id.boton1);
        botonSalvar = (Button) findViewById(R.id.boton2);
        imagebutton = (ImageButton) findViewById (R.id.imageButton);
        titulo = (TextView) findViewById(R.id.titulo);
        texto1 = (TextView) findViewById(R.id.campo2);
        texto2 = (TextView) findViewById(R.id.campo3);
        texto3 = (TextView) findViewById(R.id.campo4);
        texto4 = (TextView) findViewById(R.id.textView2);
        textoEditable = (EditText) findViewById(R.id.editText);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        radio1 = (RadioButton) findViewById(R.id.radioButton);
        radio2 = (RadioButton) findViewById(R.id.radioButton2);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        botonSalvar.setEnabled(false);

        botonVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("boton_volver", "Se apreto el boton volver");
                finish();
            }
        });

        botonSalvar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                sendclick(v);
            }
        });

    }

    public void sendclick(View v){
        String allText = new String("campo:" + textoEditable.getText());

        allText = allText + ":checkbox:";
        if(checkBox.isChecked()){
            allText=allText+"Checked:";
        }
        else{
            allText=allText+"Not checked:";
        }

        allText=allText+":toggle:";
        if(toggleButton.isChecked()){
            allText=allText + "Checked:";
        }
        else{
            allText = allText + "Not Checked:";
        }

        allText = allText + "radios:rojo:";
        String redtext="";
        if(radio1.isChecked()){
            redtext = "pulsado:";
        }
        else{
            redtext = "no pulsado:";
        }
        allText = allText + redtext;

        allText = allText + "azul";
        String bluetext = "";
        if (radio2.isChecked()) {
            bluetext = "pulsado:";
        } else {
            bluetext = "no pulsado:";
        }
        allText = allText + bluetext;

        allText = allText + "rating:";
        float f = ratingBar.getRating();
        allText = allText + Float.toString( f ) + ":";

        Log.d("app", allText);
        Toast.makeText(this, allText, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
