package com.example.guido.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class derecha extends Fragment{
    View rootView;
    TextView txt;

    //Este método debe cargar el Layout del fragmento mediante un Inflater y devolverlo.
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView= inflater.inflate (R.layout.derecha, container, false);
        txt = (TextView) rootView.findViewById(R.id.txt);

        return rootView;
    }

    public void obtenerDatos(String mensaje){
        txt.setText(mensaje);
    }

}
